# OpenML dataset: Petrleo-Brasileiro-S.A.---Petrobras-(PETR4.SA)

https://www.openml.org/d/43630

## Structure

The dataset has the following file structure:

* `dataset/`
  * `tables/`
    * [`data.pq`](./dataset/tables/data.pq): Parquet file with data
  * [`metadata.json`](./dataset/metadata.json): OpenML description of the dataset
  * [`features.json`](./dataset/features.json): OpenML description of table columns
  * [`qualities.json`](./dataset/qualities.json): OpenML qualities (meta-features)

## Description

data source
https://br.financas.yahoo.com
Content
Petrleo Brasileiro S.A. - Petrobras (PETR4.SA), from May 19, 2014 to May 16, 2019
Sao Paulo - Sao Paulo Price Postponed. Currency in BRL.
Acknowledgements
We wouldn't be here without the help of others. If you owe any attributions or thanks, include them here along with any citations of past research.
Inspiration
Your data will be in front of the world's largest data science community. What questions do you want to see answered?

## Contributing

This is a [read-only mirror](https://gitlab.com/data/d/openml/43630) of an [OpenML dataset](https://www.openml.org/d/43630). Contribute any changes to the dataset there. Alternatively, [fork the dataset](https://gitlab.com/data/d/openml/43630/-/forks/new) or [find an existing fork](https://gitlab.com/data/d/openml/43630/-/forks) to contribute to.

You can use [issues](https://gitlab.com/data/d/openml/43630/-/issues) to discuss the dataset and any issues.

For more information see [https://datagit.org/](https://datagit.org/).

